---
title: Weight of the world
author: Mel
type: post
date: 2012-03-08T05:39:52+00:00
url: /2012/03/08/weight-of-the-world/
categories:
  - Devotion
tags:
  - burdens
  - death
  - faith
  - family
  - God
  - life

---
Devotional Blog:

Burdens, 03/02/2012, Galatians 6:1-5, Romans 15:1-7

Are you a worry wort? I can be. I can worry about the most inane irrelevant things sometimes. Things I cannot control I worry about&#8230;I&#8217;m absolutely ridiculous sometimes, keeping myself awake at night worrying about things that are utterly pointless to worry about. And I worry about them at the MOST inopportune times as well&#8230;such as when I am taking off in a plane and I&#8217;m like&#8211;huh what if we crash? It&#8217;s really dumb as the statistics support me getting  pwnd by so many other causes before dying in a plane crash (1 in 7,032-lifetime odds, [Source][1]).

[<img class="aligncenter  wp-image-815" title="comic2" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/03/comic2.jpg" alt="" width="312" height="411" srcset="https://www.melaniemelendrez.com/wp-content/uploads/2012/03/comic2.jpg 520w, https://www.melaniemelendrez.com/wp-content/uploads/2012/03/comic2-227x300.jpg 227w" sizes="(max-width: 312px) 85vw, 312px" />][2]I find it funny that in the same source I have a 1 in 120,864 chance of dying by being pwnd by someones dog. In their wording&#8211;&#8220;bitten or struck by dog&#8221;. Ya that&#8217;s right, don&#8217;t you just hate it when Mitzy comes up to you and &#8216;bitch slaps&#8217; you, haha, really bad joke&#8211;but really one day, her strike could kill you!

I digress&#8230;I think I&#8217;ve made my point about pointless worrying.

<!--more-->

I&#8217;m using the same devotional entry twice as yesterday I talked about [the Bible and it&#8217;s &#8216;convincing&#8217; or lack thereof power over me][3]. What the entry wanted to focus on was actually burdens. When life gets to be too much, to take refuge in the word of God, take refuge among believers and then your burdens won&#8217;t seem so heavy.

Sometimes its so difficult to &#8216;not worry&#8217;, to not carry the burdens of family, responsibility and expectation. And for many people it extends to the world, carrying the weight of activism in areas of the environment, animal rights, politics, education&#8211;the list goes on. By not being involved you feel like an apathetic drone but to be involved adds to the burdens and expectations you already carry.

Oftentimes Christians attempt to help you by saying &#8220;Cast your cares on Jesus&#8221; (1 Peter 5:7), take heart, have faith, trust in him. Yes, ok that&#8217;s all good and fine but as the outsider that is so easy for you to say and in reality it is difficult to live out. I can verbally say or believe in my heart that a burden will go away but when I open my eyes, it&#8217;s still there, still needing to be dealt with. Saying something like that to me is effectively telling me to &#8216;wish&#8217; my burden away&#8230;the equivalent of a sympathetic pat on the back with no further offer of assistance in actually dealing with said burden. To be fair, as the outsider it is not your job to help with burdens but depending on your relationship with the person, if they are close you should try and help them and I hope you would want to help them.

As for those burdens we can do nothing about such as my inane fears/worrying, I think those have to be internally dealt with. Someone can attempt to diffuse such burdens with statistics and logic but the mind is not always logical, in fact it can be quite maddeningly illogical. What am I trying to say? I&#8217;m mental? Perhaps. I&#8217;ve flown so much in my life you&#8217;d think I had a handle on it, not so much. Ultimately I can calm myself down by reiterating that I will live as long as God&#8217;s purpose and plan for my life needs to be fullfilled, therefore any amount of turbulence won&#8217;t matter. Then of course there&#8217;s the little voice in my head (devil on the shoulder for some) that decides to mention, perhaps your time on earth IS done&#8230;DOH, leave it to my brain to be all depressive and mention something like that! SO, perhaps I just need to take some valium or ambien next time I board a plane&#8230;that way I&#8217;m either asleep or going &#8216;weeeeeeeee&#8217; with my hands up all the way down. Terribly morbid I know&#8230;but funny too.

Back to being serious&#8230;sort of. So reading this entry made me think of the burdens I feel I carry. I think my family is the biggest one. One I probably don&#8217;t have to carry but rather choose to carry. And it sounds bad to call them a &#8216;burden&#8217; and I don&#8217;t want it to sound that way. But I definitely worry and when I make decisions in my life they are near the front of the reasoning for many decisions. I carry the burden of expectations in my job. I know I am intelligent and functional but my PhD process beat all the confidence out of me, something I am slowly rebuilding. In the meantime, I worry about knowledge FAIL. That they&#8217;ll need to know something that they think I should know and I don&#8217;t know it. I know I am smart, I just don&#8217;t &#8216;feel&#8217; like the smart one.

Until recently, I used to carry the burden of neglectful friends or &#8216;frenemies&#8217;. I&#8217;m a nice person for the most part and I used to want to please everyone, make everyone happy, communicate with everyone, include everyone, help everyone&#8230;even those that would not reciprocate or worse, would take all I had to offer then bad-mouth me to others. So freakin unhealthy. I&#8217;m happy to say I&#8217;m finally releasing that burden&#8230;I think. It&#8217;s not that I don&#8217;t care&#8230;even though honestly why should I care when it comes to people like that. I guess I&#8217;m learning not to care. That sounds bad. I&#8217;m not intentionally being mean&#8211;I would wish them all the happiness in the world&#8230;I just don&#8217;t want to be in their world anymore, too tiring. And I&#8217;ve had friends cut me out for the same reason&#8230;I&#8217;m too busy to communicate or tend to their needs and they let me go as a friend and we digress to friendly acquaintances or just acquaintances. And I understand that and don&#8217;t fault them in the least. I can be a terrible friend sometimes and I try and catch it and apologize, but I never expect them to let me back in by apologizing. In their situation I&#8217;d probably do the same.

How do I deal with burdens. Well Pam (devotional writer) is correct, I do &#8216;take refuge&#8217; in Biblical teaching or counseling. Many times I call my mom and unload&#8211;lucky woman that she gets to be in those cases. Oftentimes though my burdens and worries are a result of stress. Stress that is usually taken care of by a nap, or a bath, or a glass of wine or tea &#8230;learning to unwind I think is important to managing and dealing with ones burdens. It clears your mind and allows you to see perspective. Seeking the advice and points of view of others helps level the field as well. Something that may seem so horrible and worrisome and burdensome to you, another might think lightly of and it forces you to attempt to look at it that way as well, which may help. Its funny to think that when we view others and their situations, it&#8217;s totally manageable and we tell them as much, but when we have burdens and problems, its the friggin APOCALYPSE!

I&#8217;m good at helping others not take life too seriously depending on the situation. I need to turn around and take my own advice sometimes. And for the burdens and worries that no one can control, perhaps I just need to &#8216;wish&#8217; or rather pray and have God take care of it. Hopefully then he&#8217;ll take it away or at least give me an inner peace about crashing in a fiery ball of metal or getting struck down by a dog. Actually think it&#8217;d be funnier to end up in heaven and when asked &#8216;how&#8217;d you die?&#8217; my reply would be I got &#8216;pwnd by Mitzy&#8217;.<figure id="attachment_814" style="width: 600px" class="wp-caption aligncenter">

[<img class="size-full wp-image-814" title="comic" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/03/comic.gif" alt="" width="600" height="197" srcset="https://www.melaniemelendrez.com/wp-content/uploads/2012/03/comic.gif 600w, https://www.melaniemelendrez.com/wp-content/uploads/2012/03/comic-300x98.gif 300w, https://www.melaniemelendrez.com/wp-content/uploads/2012/03/comic-500x164.gif 500w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px" />][4]<figcaption class="wp-caption-text">credit: Non sequitur comics</figcaption></figure>

 [1]: http://www.nsc.org/NSC%20Picture%20Library/News/web_graphics/Injury_Facts_37.pdf
 [2]: http://www.yellowbullet.com/forum/showthread.php?p=7524616
 [3]: http://www.melaniemelendrez.com/2012/03/07/convinced/ "convinced?"
 [4]: http://www.unicyclist.com/forums/showthread.php?p=598938