---
title: i love Edgar Allen Poe…
author: Mel
type: post
date: 2011-08-18T08:29:45+00:00
url: /2011/08/18/i-love-edgar-allen-poe/
wordbooker_options:
  - 'a:9:{s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - Poetry
tags:
  - edgar allen poe

---
<h4 style="text-align: center;">
  <strong>A Dream within a Dream</strong>
</h4>

<h4 style="text-align: center;">
  <em>By: Edgar Allen Poe</em>
</h4>

<h4 style="text-align: center;">
</h4>

<h4 style="text-align: center;">
  Take this kiss upon thy brow!
</h4>

<h4 style="text-align: center;">
  And in parting from you now,
</h4>

<h4 style="text-align: center;">
  This much let me avow-
</h4>

<h4 style="text-align: center;">
  You are not wrong, who deem
</h4>

<h4 style="text-align: center;">
  That my days have been a dream;
</h4>

<h4 style="text-align: center;">
  Yet if hope has flown away
</h4>

<h4 style="text-align: center;">
  In a night or in a day,
</h4>

<h4 style="text-align: center;">
  In a vision, or in none
</h4>

<h4 style="text-align: center;">
  Is it therefore the less gone?
</h4>

<h4 style="text-align: center;">
  All that we see or seem
</h4>

<h4 style="text-align: center;">
  Is but a dream within a dream.
</h4>

<h4 style="text-align: center;">
  <!--more-->I stand amid the roar
</h4>

<h4 style="text-align: center;">
  Of a surf tormented shore,
</h4>

<h4 style="text-align: center;">
  And I hold within my hand
</h4>

<h4 style="text-align: center;">
  Grains of the golden sand-
</h4>

<h4 style="text-align: center;">
  How few! Yet how they creep
</h4>

<h4 style="text-align: center;">
  Through my fingers to the deep
</h4>

<h4 style="text-align: center;">
  While I weep, while I weep
</h4>

<h4 style="text-align: center;">
  Oh God! Can I not grasp
</h4>

<h4 style="text-align: center;">
  Them with a tighter clasp?
</h4>

<h4 style="text-align: center;">
  Oh God! Can I not save
</h4>

<h4 style="text-align: center;">
  One from the pitiless wave?
</h4>

<h4 style="text-align: center;">
  Is all that we see or seem
</h4>

<h4 style="text-align: center;">
  But a dream within a dream?
</h4>