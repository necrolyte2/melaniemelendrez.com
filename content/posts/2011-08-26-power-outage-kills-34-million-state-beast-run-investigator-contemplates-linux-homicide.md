---
title: Power outage kills 34 million state BEAST run, Investigator contemplates Linux homicide
author: Mel
type: post
date: 2011-08-26T03:05:59+00:00
url: /2011/08/26/power-outage-kills-34-million-state-beast-run-investigator-contemplates-linux-homicide/
wordbooker_options:
  - 'a:8:{s:18:"wordbook_noncename";s:10:"08439e6799";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";}'
categories:
  - I will do Science to it!
tags:
  - Asia
  - BEAST
  - phylogenetics
  - process
  - software

---
As the title suggests I walked into the office this morning to two computers&#8230;both shut off. Now no one touches my desk especially when I have analysis running and we had a huge storm last night with lightening and wind and although Bangkok is a &#8216;1st&#8217; world entity of itself some days the fact of the matter is, I live in a 3rd world country that is 98% agriculture. The result? 34,510,000 iterations of a BEAST run of 100 million completed and the analysis ends with the power cutting off. My poor linux, diligently plowing away at iteration after iteration, only to be thwarted when its attachment to life power is cut.

CURSES!

A check in tracer reveals that my posterior, prior, likelihood, tree likelihood and coalescent are all at ESS values in the red (under 100) and clock.rate is under 200 (in the yellow). So things have not finished &#8216;mixing&#8217; and converging altogether.

DOUBLE CURSES!

Thankfully I started the same run on the [Oslo Bioportal][1] August 23, 2011 and currently the status is on resource/started. Unfortunately they do not give me an up to the moment snapshot of my analysis and I cannot check using Tracer with it along the way so it will march to 100 million generations until it is done. I was hoping to track my run as it went to see if it exhibited good mixing and convergence prior to 100 million iterations&#8211; but alas, I will never know. And if Oslo has a power outage&#8211;then the &#8216;gnomes&#8217; or &#8216;gremlins&#8217; are up to something &#8230; little bastards.

There&#8217;s something comforting about watching status lines flip by on an analysis, lets you know the computer is hammering away at the brick wall of analysis trying to find that one loose brick. That&#8217;s why I always like to run things on the terminal although I have to say the GUI for the program is quite pretty. Yes, I just called the GUI for a Bayesian analysis program &#8216;pretty&#8217;&#8211; sometimes I indulge in superficial scientific vanity.

So I&#8217;m torn on whether to restart the analysis from iteration 0 or wait for Oslo to cough up my results. Without status lines to watch much of my previous [list of things to do while BEAST runs][2] gets nullified&#8230; I guess the &#8216;analyst&#8217; will have to actually go outside&#8230; see the sun&#8230; get some fresh &#8216;non-circulated&#8217; air&#8230; engage in social behavior with others perhaps also affected, having lost analysis down the black hole of power outages and now venturing forth confused from their cave like offices into the bright heat of Bangkok&#8230; or I need to find a new program with status lines that takes a long time to run!

~cheers.

 [1]: https://www.bioportal.uio.no/
 [2]: http://www.melaniemelendrez.com/2011/08/19/things-to-do-while-beast-runs/ "Things to do while BEAST runs"