---
title: Neutrinos
author: Mel
type: post
date: 2011-09-29T01:55:08+00:00
url: /2011/09/29/neutrinos/
wordbooker_options:
  - 'a:11:{s:18:"wordbook_noncename";s:10:"8e34a7574a";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:20:"wordbook_comment_get";s:2:"on";s:17:"wordbook_new_post";s:1:"1";}'
wordbooker_thumb:
  - http://imgs.xkcd.com/comics/neutrinos.png
wordbooker_extract:
  - |
    So in case you've been living under a rock there's a huge hub bub about neutrinos breaking the speed of light. 
    
    Here's the published work.
    
    I don't have much to add as I'm not a physicist, but I think it's cool that they are offering to 'eat short ...
categories:
  - I will do Science to it!

---
So in case you&#8217;ve been living under a rock there&#8217;s a huge hub bub about [neutrinos breaking the speed of light.][1] 

Here&#8217;s the [published work][2].

I don&#8217;t have much to add as I&#8217;m not a physicist, but I think it&#8217;s cool that they are offering to &#8216;[eat shorts][3]&#8216; if the calcs are right.

and pretty much this&#8230;<figure style="width: 740px" class="wp-caption aligncenter">

<img title="Neutrinos" src="http://imgs.xkcd.com/comics/neutrinos.png" alt="" width="740" height="232" /><figcaption class="wp-caption-text">credit www.xkcd.com</figcaption></figure>

 [1]: http://www.bbc.co.uk/news/science-environment-15038826
 [2]: http://arxiv.org/abs/1109.4897
 [3]: http://news.bbc.co.uk/2/hi/programmes/newsnight/9598802.stm