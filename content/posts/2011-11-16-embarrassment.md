---
title: Embarrassment
author: Mel
type: post
date: 2011-11-16T04:53:23+00:00
url: /2011/11/16/embarrassment/
categories:
  - Devotion
tags:
  - embarrassment
  - God
  - high school

---
Devotional Blog:

Topic: &#8220;Embarrassed&#8221;, 11/15/2011, Proverbs 22:4-16

So I haven&#8217;t blogged about the devotions I&#8217;ve been reading and yes, I have been reading them because so far they&#8217;ve been records on repeat, dealing with issues already discussed in the book&#8230;and I&#8217;ve only been reading since August. So I&#8217;m wondering at this point how many topics she&#8217;s going to deal with multiple times. I finally came across some more original ones, like yesterday&#8217;s.

In high school I had the meanest crush on a kid who was my friend freshman and sophomore year then went off that summer and got all buff from working out, pretty much lost all the acne and came back junior year HOT. With his newfound hotness and suddenly getting plenty of attention our friendship fell off to the side and while we were always friendly, it just was different now. I wasn&#8217;t exactly a hottie back then and therefore couldn&#8217;t exactly &#8216;compete&#8217; for his affections. Prior to that, it was the classic one is interested while the other is not and vice a versa.

Anyway, during my crush phase I was walking around campus and saw him so of course I prolonged my look&#8211;just long enough to run smack into the middle of a pillar, hard enough it landed me on my ass. In the middle of morning break when the high school campus was full of kids (our campus wasn&#8217;t closed, it was several small buildings you had to walk between). And I wanted to crawl into a corner and die&#8230;.of course.

Another time in high school, I saw my friends across the field at another building, they saw me, we waved and I jogged toward them&#8230;only to be hit by a golf cart, campus security. They weren&#8217;t paying attention, I wasn&#8217;t paying attention&#8230;it culminated on me &#8216;bug-splatting&#8217; myself onto the golf cart and &#8216;melting&#8217; off like in a cartoon while my friends laughed hysterically. I was a very accident prone child apparently.

Even as an adult I have my moments of lip syncing or tap dancing in the lab while experiments are running when I think no one is looking only to turn around and have an audience! DOH!

Such are these small embarrassments. I related this on facebook and actually got some pretty funny stories in response:

  * I once mooned my husband when we were playing racquetball &#8211; completely forgetting the court had a see through glass door! 🙂
  * When I had a stand up desk in he CCB I would listen to music and do little dances while I worked. Well I had forgotten that I had a meeting with a couple campus people and didn&#8217;t realize they had been standing there for a couple minutes watching me &#8220;get jiggy&#8221;
  * It happened several times that I was in the bus &#8211;crowded with people&#8211; and then I had an idea about solving a problem I had been thinking about for quite a while. Then I shouted &#8220;That&#8217;s why!&#8221; in a very loud voice. People would immediatly stare at me&#8230;.
  * I pee in the forest when no one is looking. 🙂
  * On more than one occasion I&#8217;ve been extreme-coding with Pandora blasting when my advisor came in to the lab to chat&#8230; He starting clapping loudly to get my attention&#8230;

Good to know I&#8217;m not the only one.

Reflecting back all these situations are funny, though they were just shy of mortifying back then and of course EVERYTHING is more dramatic to a high school student. Funny thing is, some of us aren&#8217;t much better in adulthood, haha!

Its good to be reminded of humility. Speaking with Tyghe one time he said he thinks God throws situations at us just so he can laugh at our antics and reactions dealing with said situation. Or perhaps he doesn&#8217;t actually throw a situation but he&#8217;s certainly up there shaking his head and laughing for sure and most things us humans do.