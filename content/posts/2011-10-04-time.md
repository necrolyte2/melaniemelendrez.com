---
title: time
author: Mel
type: post
date: 2011-10-04T14:13:02+00:00
url: /2011/10/04/time/
wordbooker_options:
  - 'a:11:{s:18:"wordbook_noncename";s:10:"504f210c62";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:20:"wordbook_comment_get";s:2:"on";s:17:"wordbook_new_post";s:1:"1";}'
wordbooker_extract:
  - |
    A list of how I will not spend my time:
    
    	worrying about what others think
    	holding grudges
    	drinking bad wine
    	finding ways to 'avenge' my hurts from others
    	cuddling things that are not soft
    	arguing illogical people
    	trying to be someone i a ...
categories:
  - Devotion
  - Life or something like it

---
A list of how I _**will not**_ spend my time:

  1. worrying about what others think
  2. holding grudges
  3. drinking bad wine
  4. finding ways to &#8216;avenge&#8217; my hurts from others
  5. cuddling things that are not soft
  6. arguing illogical people
  7. trying to be someone i am not
  8. allowing the stress that came up during my Ph.D. to ever enter my life again
  9. eating Lima beans or mayonnaise
 10. picking on myself

These are the things I will not spend time on&#8230;whether it actually happens remains to be seen but sometimes it&#8217;s interesting to put down a &#8216;not to do list&#8217; rather than a &#8216;to do list&#8217;.