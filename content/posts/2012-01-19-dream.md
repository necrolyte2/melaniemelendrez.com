---
title: Dream within a dream
author: Mel
type: post
date: 2012-01-19T06:49:12+00:00
url: /2012/01/19/dream/
categories:
  - Devotion
tags:
  - dreams
  - edgar allen poe
  - faith
  - family
  - God
  - living
  - poetry
  - timing

---
Devotional Blog:

Topic: &#8220;Pretty Good Company&#8221;, 12/29/11, John 12: 20-28

Happy Birthday [Edgar Allen Poe, Born January 19, 1809.][1] Odd way to start a devotional post complete with topic title and Bible verse by saying happy birthday to a man whose poetry and stories are often of the macabre gothic nature and depressing, but now that I&#8217;ve piqued your curiosity, stick with me&#8230;

Today&#8217;s entry is about walking into the dreams that God has given us in our lives. The visions, the promises, the hopes&#8230;and perhaps not getting to see or experience the fruits of our labors, our suffering, our patience. The author (Pam) goes onto to say, &#8216;you are not alone&#8217;. You are not the only one to receive great promises only to never see them come to pass in your lifetime or as Moses did, stand at the border and watch your people walk into the promise led by another man. How heinously frustrating. You do everything you believe God is telling you to do, you walk through the doors, you invest time, faith, money, more time, more faith&#8230;.you sit and watch as others experience the joy that comes from their dreams or promises coming to pass and you sit. You sit, telling yourself to be patient, telling yourself God has not forgotten you, telling yourself that you want things in God&#8217;s timing. Then you look up and you say God WHEN is your timing!!!???  And you cry out&#8230;you cry out.<!--more-->

> **A Dream Within a Dream by Edgar Allen Poe**
> 
> &nbsp;
> 
> Take this kiss upon the brow!
  
> And, in parting from you now,
  
> Thus much let me avow-
  
> You are not wrong, who deem
  
> That my days have been a dream;
  
> Yet if hope has flown away
  
> In a night, or in a day,
  
> In a vision, or in none,
  
> Is it therefore the less gone?
  
> All that we see or seem
  
> Is but a dream within a dream.
> 
> &nbsp;
> 
> I stand amid the roar
  
> Of a surf-tormented shore,
  
> And I hold within my hand
  
> Grains of the golden sand-
  
> How few! yet how they creep
  
> Through my fingers to the deep,
  
> While I weep- while I weep!
  
> O God! can I not grasp
  
> Them with a tighter clasp?
  
> O God! can I not save
  
> One from the pitiless wave?
  
> Is all that we see or seem
  
> But a dream within a dream?

&#8220;Oh God can I not grasp them with a tighter clasp?&#8221;

> &#8220;The truth is, a kernel of wheat must be planted in the soil. Unless it dies it will be a lone&#8211;a single seed. But its death will produce many new kernels&#8211;a plentiful harvest of new lives.&#8221; -NIV, John 12:24

Often times we grasp onto a dream, someone, something so tightly we do not allow it to carry out the purpose it was deemed for. We must let it go. Just like with seeds, it must be released and planted to bear fruit and produce more seeds. It may seem by letting go you are letting your dreams and hopes slip into obscurity.

> &#8220;Grains of the golden sand-
  
> How few! yet how they creep
  
> Through my fingers to the deep,
  
> While I weep- while I weep!&#8221;

But rather you are releasing yourself from having to &#8216;make&#8217; the dream happen. You are acknowledging that you&#8217;ve done all you can and if things are going to change, going to happen it&#8217;s got to be God, if anything, it just can&#8217;t be you anymore.

I have been present when prophets have prayed over me and my family. I have heard the promises the &#8216;plans&#8217; that they say God has for my family. I&#8217;ve watched my family struggle, I&#8217;ve watched my family pray and cry out, I&#8217;ve prayed, I&#8217;ve watched as hopes get dashed, I&#8217;ve watched as injustice has reigned down on them. I&#8217;ve watched God&#8217;s mercy in supernaturally providing&#8230;.and I say supernatural provision in that there&#8217;s no logical or evident reason for the provision it just happens with no strings attached. I never questioned provision as a child, we learned just to be thankful for it. Often times I wonder and ask God if my family will ever walk into what he&#8217;s supposedly got planned for them according to so many pastors and prophets.

Often times there are more things at work in the background then we could possibly imagine. We see only a small part of the big picture, we are, after all, only human with a finite understanding. I&#8217;ve gotten called out for &#8216;questioning&#8217; God. I don&#8217;t question his existence, I question his timing&#8211;his methods. Perhaps I shouldn&#8217;t, but I do. Even though I know whatever timing or he&#8217;s doing is probably the best course of action in the end, I still can&#8217;t help but look up and go &#8216;Wha? Why?&#8221; In my nature, probably why I&#8217;m a scientist&#8211;I question everything. Its not because I lack faith, though others would argue otherwsie. Perhaps I don&#8217;t hear God like others because I don&#8217;t have a &#8216;deep enough relationship&#8217; a &#8216;clear enough heart and mind&#8217;&#8230;perhaps because I&#8217;m too tuned into the &#8216;world&#8217; or the &#8216;logical&#8217;; essentially turning myself into the antithesis of faith. These are all things I&#8217;ve been told throughout my faith.

And yes, I do question God, again, not his existence but rather I don&#8217;t understand or hear from him regularly like other Christians apparently do. My faith says one thing, my reason says something else. But one thing never changes&#8230;

I don&#8217;t ever doubt him.

So what are the choices&#8230;depression, anger, crying out at the injustice, feeling like we are alone in our suffering, alone in the world. Pam illustrates that in the Bible this path was walked by more than just you; David who gathered everything to build God&#8217;s temple, only to know his son Solomon would build it instead, or perhaps Stephen a martyr stoned so early in his mission that he never got to see the church that he&#8217;d fought to create and believed in so much&#8230;

So what are our choices? Perhaps we could continue to sit, continue to wait, continue to pray, continue to hope. For how long? Your guess is as good as mine but it helps to know we don&#8217;t walk alone and in that, we may strengthen our faith and continue to believe, continue to dream&#8230;

 [1]: http://en.wikipedia.org/wiki/Edgar_Allan_Poe